

<?php
   /*template name:homepage*/
?>



<html>
    <head>
		<link rel="stylesheet" href="<?php bloginfo ('template_url'); ?>/css/style.css" type="text/css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta charset="<?php bloginfo('charset'); ?>">
		<link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400&family=Roboto:wght@300&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,500;1,400&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@1,300&family=Roboto:ital,wght@0,500;1,400&display=swap" rel="stylesheet">
    </head>
	<body>
		<section class="header">
		    <div class="container">
			   <div class="logo">
				   <p class="nada"><?php the_field('nada'); ?></p>
				   <p><?php the_field('macura'); ?></p>
			   </div>
			   <div class="menu">
				   <nav>
						<ul>
							<a href="#"><li><?php the_field('homepage'); ?></li></a>    
							<a href="#"><li><?php the_field('about'); ?></li></a>    
							<a href="#"><li><?php the_field('services'); ?></li></a>    
							<a href="#"><li><?php the_field('references'); ?></li></a>    
							<a href="#"><li><?php the_field('blog'); ?></li></a>    
							<a href="#"><li><?php the_field('contact'); ?></li></a>    
							<a href="#"><li class="sr"><?php the_field('sr'); ?></li></a>    
							<a href="#"><li class="en"><?php the_field('en'); ?></li></a>    
						</ul>
				   </nav>
				   <div class="menu-toggle"><img src="<?php the_field('toogle'); ?>"/></div>  
				   <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
					<script type="text/javascript">
							$(document).ready(function(){
								$('.menu-toggle').click(function(){
									$('nav').toggleClass('active')
								})
							})
					</script>
			   </div>
			</div>   
		</section>
		<section class="section one">
			<div class="note">
				<p><?php the_field('note'); ?></p>
			</div>
			<div class="heading">
				<div class="name">
					<h1><?php the_field('name'); ?></h1>
				</div>		
				<div class="text">
					<p><?php the_field('text'); ?></p>
				</div>
				<div class="tell">
					<a href="#"><p><?php the_field('tell'); ?></p></a>
				</div>
			</div>
		</section>
		<section class="section two">
			<div class="wall">
				<div class="prime">
					<div class="quatro">
						<div class="triangle">
							<img src="<?php the_field('triangle'); ?>"/>
							<div class="cube">
								<img src="<?php the_field('cube'); ?>"/>
							</div>   
						</div>
					</div>	
					<div class="tent">
						<div class="parol">
							<h1><?php the_field('parol'); ?></h1>
						</div>
						<div class="paroles">
							<p><?php the_field('paroles'); ?></p>
						</div>
					</div>	
					<div class="sport">
						<div class="sport-one">
							<div class="globe">
								<img src="<?php the_field('handball'); ?>"/>
								<p><?php the_field('hand'); ?></p>
							</div>
							<div class="globe">
								<img src="<?php the_field('football'); ?>"/>
								<p><?php the_field('foot'); ?></p>
							</div>
							<div class="globe">
								<img src="<?php the_field('valleyball'); ?>"/>
								<p><?php the_field('valley'); ?></p>
							</div>
							<div class="globe">
								<img src="<?php the_field('basket'); ?>"/>
								<p><?php the_field('bask'); ?></p>
							</div>
							<div class="globe bulit">
								<img src="<?php the_field('shooting'); ?>"/>
								<p><?php the_field('shoot'); ?></p>
							</div>
						</div>
						<div class="sport-two">
							<div class="inter">
								<img src="<?php the_field('judo'); ?>"/>
								<p><?php the_field('jud'); ?></p>
							</div>
							<div class="inter">
								<img src="<?php the_field('karate'); ?>"/>
								<p><?php the_field('kar'); ?></p>
							</div>
							<div class="inter">
								<img src="<?php the_field('tennis'); ?>"/>
								<p><?php the_field('tenn'); ?></p>
							</div>
							<div class="inter">
								<img src="<?php the_field('athletics'); ?>"/>
								<p><?php the_field('athle'); ?></p>
							</div>
							<div class="inter swam">
								<img src="<?php the_field('biathlon'); ?>"/>
								<p><?php the_field('biath'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</section>
		<section class="section three">
			<div class="triangle-white">
				<div class="only">
					<img  src="<?php the_field('only'); ?>"/>
                </div>
				<div class="cube-blue">
					<img  src="<?php the_field('cube-blue'); ?>"/>
				</div>
			</div>		
			<div class="player">
				<img src="<?php the_field('player'); ?>"/>
			</div>
		</section>
		<section class="section four">
			<div class="textualite">
				<div class="introduct">
					<p><?php the_field('introduct'); ?></p>
				</div>
				<div class="quado">
					<div class="twint">
						<div class="lope one">
							<h3><?php the_field('lope-one'); ?></h3>
							<p><?php the_field('lope-one-one'); ?></p>
						</div>
						<div class="lope two">
							<h3><?php the_field('lope-two'); ?></h3>
							<p><?php the_field('lope-two-two'); ?></p>
						</div>
						<div class="lope three">
							<h3><?php the_field('lope-three'); ?></h3>
						</div>
						<div class="lope four">
							<h3><?php the_field('lope-four'); ?></h3>
						</div>
					</div>
					<div class="twint">
						<div class="lope five">
							<h3><?php the_field('lope-five'); ?></h3>
							<p><?php the_field('lope-five-five'); ?></p>
						</div>
						<div class="lope six">
							<h3><?php the_field('lope-six'); ?></h3>
						</div>
						<div class="lope seven">
							<h3><?php the_field('lope-seven'); ?></h3>
						</div>
						<div class="lope eight">
							<h3><?php the_field('lope-eight'); ?></h3>
							<p><?php the_field('lope-eight-eight'); ?></p>
						</div>
					</div>
				</div>
				<div class="seecoll">
					<a href="#"><p>All services</p></a>
				</div>	
			</div>		
		</section>
		<section class="victory">
			<div class="famos">
				<img src="<?php the_field('famos'); ?>"/>
			</div>	
		</section>
		<section class="section five">
			<div class="coach">
				<p><?php the_field('coach'); ?></p>
			</div>
			<div class="homes">
			    <div class="device">
					<div class="cubice">
						<img src="<?php the_field('cubice'); ?>"/>
					</div>
					<div class="small-cube">	
						<img src="<?php the_field('small-cube'); ?>"/>
					</div>
					<div class="small-cube">	
						<img src="<?php the_field('small-cube'); ?>"/>
					</div>
					<div class="small-cube">	
						<img src="<?php the_field('small-cube'); ?>"/>
					</div>
					<div class="small-cube">	
						<img src="<?php the_field('small-cube'); ?>"/>
					</div>
					<div class="small-cube">	
						<img src="<?php the_field('small-cube'); ?>"/>
					</div>
					<div class="small-cube">	
						<img src="<?php the_field('small-cube'); ?>"/>
					</div>
					<div class="small-cube">	
						<img src="<?php the_field('small-cube'); ?>"/>
					</div>
					<div class="small-cube">	
						<img src="<?php the_field('small-cube'); ?>"/>
					</div>
					<div class="small-cube">	
						<img src="<?php the_field('small-cube'); ?>"/>
					</div>
					<div class="small-cube">	
						<img src="<?php the_field('small-cube'); ?>"/>
					</div>
			    </div>
			    <div class="reflect">
					<h3><?php the_field('reflect'); ?></h3>
					<p class="ref-text"><?php the_field('ref-text'); ?></p>
					<p class="JD"><?php the_field('jd'); ?></p>
					<p class="china"><?php the_field('china'); ?></p>
					<div class="servile">
						<a href="#"><p><?php the_field('servile'); ?></p></a>
					</div>
			    </div>
		   </div>
	       <div class="image-box">
				<div class="direct">
					<img src="<?php the_field('direct'); ?>"/>
				</div>
				<div class="triangle-right">
					<div class="bigest">
						<img  src="<?php the_field('bigest'); ?>"/>
					</div>  
					<div class="cube-midlee">
						<img  src="<?php the_field('cube-midlee'); ?>"/>
					</div>
				</div>
		   </div>				
		</section>
		<section class="section six">
			<div class="wall-two"></div>
			<div class="coach-two">
				<div class="image-box-two">
					<div class="triangle-left">
						<div class="triangle-blue">
							<img src="<?php the_field('triangle-blue'); ?>"/>
						</div>
						<div class="cube-white">
							<img src="<?php the_field('cube-white'); ?>"/>
						</div>
					</div>
					<div class="direct-two">
						<img src="<?php the_field('direct-two'); ?>"/>
					</div>
				</div>
				<div class="reflect-two">
					<div class="intimer">
						<h3><?php the_field('aboutme'); ?></h3>
						<p class="there"><?php the_field('intimer'); ?></p>
						<div class="geting">
							<a href="#"><p><?php the_field('geting'); ?></p></a>
						</div>
					</div>
				</div>	
			</div>		
		</section>
	    <section class="section seven">
			<div class="blog">
				<h2><?php the_field('latest'); ?></h2>
			</div>
			<div class="track">
				<div class="galery">
					<div class="part-left">
						<img src="<?php the_field('part-left'); ?>"/>
						<div class="tex-slk">
							<p class="date"><?php the_field('date'); ?></p>
							<p class="tk"><?php the_field('tk'); ?></p>
						</div>
					</div>
					<div class="part-right">
						<div class="half-up">
							<img src="<?php the_field('half-up'); ?>"/>
							<div class="tex-slk">
								<p class="date"> <?php the_field('date'); ?></p>
								<p class="tk"><?php the_field('tk'); ?></p>
							</div>
						</div>
						<div class="half-down">
							<div class="bicucle">
								<img src="<?php the_field('bicucle'); ?>"/>
								<div class="tex-slk">
									<p class="date"> <?php the_field('date'); ?></p>
									<p class="tk"><?php the_field('tkk'); ?></p>
								</div>
							</div>
							<div class="winner">
								<img src="<?php the_field('winner'); ?>"/>
								<div class="tex-slk">
									<p class="date"><?php the_field('date'); ?></p>
									<p class="tk"><?php the_field('tkk'); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="section eight">
			<div class="ball">
				<div class="lecture">
					<h2><?php the_field('lecture'); ?></h2>
					<div class="contac">
						<a href="#"><p><?php the_field('contac'); ?></p></a>
					</div>
				</div>
			</div>	
		</section>
		<section class="footer">
			<div class="footer-version-one">
				<div class="footer-part-left">
					<div class="logo-down">
						<p class="nada"><?php the_field('nada-down');?></p>
						<p><?php the_field('macura-down');?></p>
					</div>
					<div class="block-border"></div>	
					<div class="copy">
						<p><?php the_field('copy');?><a href="#"><?php the_field('neki-link'); ?></a> I <a href="#"><?php the_field('neki-neki-link');?></a></p>
					</div>
				</div>
				<div class="top">
					<div class="base">
						<img class="piramida" src="<?php the_field('base'); ?>"/>
					</div>
					<div class="flag">
						<img  src="<?php the_field('flag'); ?>"/>
					</div>
				</div>
				<div class="footer-part-right">
					<div class="menu-social">
						<div class="footer-menu">
							<ul>
								<li class="homepage"><a href="#"><?php the_field('footer-homepage'); ?></a></li>
								<li><a href="#"><?php the_field('footer-about'); ?></a></li>
								<li><a href="#"><?php the_field('footer-services'); ?></a></li>
								<li><a href="#"><?php the_field('footer-references'); ?></a></li>
								<li><a href="#"><?php the_field('footer-blog'); ?></a></li>
								<li><a href="#"><?php the_field('footer-contac'); ?></a></li>
							</ul>
						</div>
						<div class="footer-social">		
							<ul>
								<li><a href="#"><img src="<?php the_field('instagram'); ?>"/></a></li>
								<li><a href="#"><img src="<?php the_field('facebook'); ?>"/></a></li>
								<li><a href="#"><img src="<?php the_field('linked'); ?>"/></a></li>
							</ul>	
						</div>
					</div>	
					<div class="block-border-1"></div>	   
					<div class="desing">
						<p><?php the_field('desing'); ?><a href="#"><?php the_field('pop-art'); ?></a></p>
					</div>		
				</div>
			</div>
			<div class="footer-s">
				<div class="prva">
					<div class="logo-down">
						<p class="nada"><?php the_field('nada-down');?></p>
						<p><?php the_field('macura-down');?></p>
					</div>
					<div class="footer-menu">
						<ul>
							<li class="homepage"><a href="#"><?php the_field('footer-homepage'); ?></a></li>
							<li><a href="#"><?php the_field('footer-about'); ?></a></li>
							<li><a href="#"><?php the_field('footer-services'); ?></a></li>
							<li><a href="#"><?php the_field('footer-references'); ?></a></li>
							<li><a href="#"><?php the_field('footer-blog'); ?></a></li>
							<li><a href="#"><?php the_field('footer-contac'); ?></a></li>
						</ul>
					</div>
				</div>
				<div class="druga">
					<div class="block-border"></div>
				</div>
				<div class="treca">
				    <div class="copy">
						<p><?php the_field('copy');?><a href="#"><?php the_field('neki-link'); ?></a> I <a href="#"><?php the_field('neki-neki-link');?></a></p>
					</div>
					<div class="desing">
						<p><?php the_field('desing'); ?><a href="#"><?php the_field('pop-art'); ?></a></p>
					</div>
				</div>
			</div>	
		</section>
	</body> 
</html>

<script>
	$(window).scroll(function() {
		var theta = $(window).scrollTop() / 20 % Math.PI;
		$('.cube-blue').css({ transform: 'rotate(' + theta + 'rad)' });
		$('.cube-midlee').css({ transform: 'rotate(' + theta + 'rad)' });
		$('.cube').css({ transform: 'rotate(' + theta + 'rad)' });
		$('.cube-white').css({ transform: 'rotate(' + theta + 'rad)' });
	});
</script>