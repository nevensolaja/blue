<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'blue' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '$XN,MtF2o;=[CZfoiGT}r>|4b@O%GhD{D[)6cn`@e|uxzAos*6x{kKM.i{x=[-i9' );
define( 'SECURE_AUTH_KEY',  'wp[myNI|;GNgs(A(|<AnUvjIojR4)25i/Ya9ZXAYfmzn4%ls*%DbE8LtXz7/}AsU' );
define( 'LOGGED_IN_KEY',    'wf]I*lFaQJKPjmYu9B,{WPXwePrY&@-CKC@{*F)84jyEP.lAJ7tjKz-:Ga(>Np;L' );
define( 'NONCE_KEY',        'gs *;HLRJ>9h]VpLyktbzLIryyFnw5T;q,K8>f!.wri_=_@fsj/= *yfE`m)fEuf' );
define( 'AUTH_SALT',        '>MG;&Xo+8D1B<LX9sfM~`NXAad1`:m.$2Sn$B(0$ bEPb(:U8z5$RsB2&kXR2 ;#' );
define( 'SECURE_AUTH_SALT', 'z5=^y%%tS7>=1XFe|>~wV1_d9(tJ~ZgjMujEyz8[:[O3jH}zuR*[sr7OlJ2%cX[S' );
define( 'LOGGED_IN_SALT',   ' HFseF][1SytAzW9#>wFYPT:goM*xe41W9RW(ZBn$Ain(,h82]4a.y~)[38UG@,]' );
define( 'NONCE_SALT',       'P4zA !n}/d^3PU!T?n5oo?xjjW]P#96D!fJ824#es8@>hDafi/Kf:LUd*TJ|ZgR+' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
